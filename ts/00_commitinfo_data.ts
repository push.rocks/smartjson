/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartjson',
  version: '5.0.6',
  description: 'typed json handlers'
}
